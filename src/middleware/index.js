// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
    const authenticateModuleConfigure = require ('../middleware/authenticateConfigure.js');
    authenticateModuleConfigure(app, 'login');
    
  // Add your custom middleware here. Remember that
  // in Express, the order matters.
};
