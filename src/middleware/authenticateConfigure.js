function main (app, authpath)
{
// authpath - route for local authentication service 
const auth = require('@feathersjs/authentication');
const local = require('@feathersjs/authentication-local');
const express = require('@feathersjs/express');    
const memory = require('feathers-memory'); 
const errors = require('@feathersjs/errors');
       
local.options = {
    secret: 'figVamVsemANeSecret', // the secret for hashing password
    name: 'local', // the name to use when invoking the authentication Strategy
    entity: 'user', // the entity that you're comparing username/password against
    service: 'users', // the service to look up the entity
    usernameField: 'email', // key name of username field
    passwordField: 'password', // key name of password field
    entityUsernameField: 'email', // key name of the username field on the entity (defaults to `usernameField`) 
    entityPasswordField: 'password', // key name of the password on the entity (defaults to `passwordField`) 
    passReqToCallback: true, // whether the request object should be passed to `verify`
    session: false, // whether to use sessions,
    jwt: {
            secretOrPrivateKey: 'figVamVsemANeSecret', 
            header: { typ: 'access' }, // by default is an access token but can be any type. This is not a typo!
            audience: 'http://localhost:3030', // The resource server where the token is processed
            subject: 'anonymous', // Typically the entity id associated with the JWT
            issuer: 'feathers', // The issuing server, application or resource
            algorithm: 'HS256', // the algorithm to use
            expiresIn: '1d' // the access token expiry
        },
    Verifier: local.Verifier
}

// Authentication method get jwt from header.authorization
local.authenticateJWT = async function (hook) {
  const token = hook.params.headers.authorization;
  try {
      await hook.app.passport.verifyJWT(token, local.options);
  } catch (error) {      
    throw new errors.NotAuthenticated({ message: error.message });
  }
  return hook
};    

app.configure(express.rest());
app.configure(auth(local.options));
app.configure(local());
                 
app.use('/'+authpath, memory());        
    
app.service(authpath).hooks({
  before: {
    create: [auth.hooks.authenticate(['local', 'jwt'])]
  },
  after: { 
    create: [ local.hooks.protect('password')]
  }  
});   

app.service('authentication').hooks({
 before: {
  create: [
   // You can chain multiple strategies
   auth.hooks.authenticate('local', 'jwt')
  ],
  remove: [
   auth.hooks.authenticate('jwt')
  ]
 }
});
    
}
module.exports = main;


/*
local.authenticateJWT2= function  (){
return async hook=>{
  const token = hook.params.headers.authorization;
  try {
      await hook.app.passport.verifyJWT(token, local.options);
  } catch (error) {      
    throw new errors.NotAuthenticated({ message: error.message });
  }
  return hook
}
};   
*/